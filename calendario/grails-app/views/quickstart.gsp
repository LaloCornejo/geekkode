 <!DOCTYPE html>
<html>
  <head>
    <title>Google Calendar API Quickstart</title>
    
    <meta charset='utf-8' />
    
    <g:external dir = "stylesheets" file = "fullcalendar.min.css"/>
    <link href="../fullcalendar.print.min.css" rel = "stylesheet" media = "print"/>
    <g:external dir = "javascripts" file = "jquery-2.2.0.min.js"/>
    <g:external dir = "javascripts" file = "moment.js"/>
    <g:external dir = "javascripts" file = "fullcalendar.min.js"/>
    <g:external dir = "javascripts" file = "gcal.js"/>
    
    

    <style>
      #calendar {
        max-width: 600px;
        margin: 0 auto;
      }

     #loading {
        display: none;
        position: absolute;
        top: 10px;
        right: 10px;
      }

    </style>
  </head>
  <body>
    <p>Google Calendar API Quickstart</p>

    <!--Add buttons to initiate auth sequence and sign out-->
    <button id="authorize-button" style="display: none;">Authorize</button>
    <button id="signout-button" style="display: none;">Sign Out</button>

    <pre id="content"></pre>

    <script type="text/javascript">
      // Client ID and API key from the Developer Console
      var CLIENT_ID = '511601860901-dv0mqau6o75eu96f290i101cd3t38qk3.apps.googleusercontent.com';
      var API_KEY = 'AIzaSyDbbQ-FxT378bBPFyee3fQwsyhNV04KLKM'; 

      // Array of API discovery doc URLs for APIs used by the quickstart
      var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

      // Authorization scopes required by the API; multiple scopes can be
      // included, separated by spaces.
      var SCOPES = "https://www.googleapis.com/auth/calendar.readonly";

      var authorizeButton = document.getElementById('authorize-button');
      var signoutButton = document.getElementById('signout-button');

      /**
       *  On load, called to load the auth2 library and API client library.
       */
      function handleClientLoad() {
        gapi.load('client:auth2', initClient);
      }

      /**
       *  Initializes the API client library and sets up sign-in state
       *  listeners.
       */
      function initClient() {
        gapi.client.init({
          apiKey: API_KEY,
          clientId: CLIENT_ID,
          discoveryDocs: DISCOVERY_DOCS,className: 'gcal-event'
          scope: SCOPES
        }).then(function () {
          // Listen for sign-in state changes.
          gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

          // Handle the initial sign-in state.
          updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
          authorizeButton.onclick = handleAuthClick;
          signoutButton.onclick = handleSignoutClick;
        });
      }

      /**
       *  Called when the signed in status changes, to update the UI
       *  appropriately. After a sign-in, the API is called.
       */
      function updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
          authorizeButton.style.display = 'none';
          signoutButton.style.display = 'block';
          listUpcomingEvents();
        } else {
          authorizeButton.style.display = 'block';
          signoutButton.style.display = 'none';
        }
      }

      /**
       *  Sign in the user upon button click.
       */
      function handleAuthClick(event) {
        gapi.auth2.getAuthInstance().signIn();
      }

      /**
       *  Sign out the user upon button click.
       */
      function handleSignoutClick(event) {
        gapi.auth2.getAuthInstance().signOut();
      }

      /**
       * Append a pre element to the body containing the given message
       * as its text node. Used to display the results of the API call.
       *
       * @param {string} message Text to be placed in pre element.
       */
      function appendPre(message) {
        var pre = document.getElementById('content');
        var textContent = document.createTextNode(message + '\n');
        pre.appendChild(textContent);
      }

      /**
       * Print the summary and start datetime/date of the next ten events in
       * the authorized user's calendar. If no events are found an
       * appropriate message is printed.
       */
      function listUpcomingEvents() {
        gapi.client.calendar.events.list({
          'calendarId': 'primary',
          'timeMin': (new Date()).toISOString(),
          'showDeleted': false,
          'singleEvents': true,
          'maxResults': 10,
          'orderBy': 'startTime'
        }).then(function(response) {
          var events = response.result.items;
          appendPre('Upcoming events:');

          if (events.length > 0) {
            for (i = 0; i < events.length; i++) {
              var event = events[i];
              var when = event.start.dateTime;
              if (!when) {
                when = event.start.date;
              }
              appendPre(event.summary + ' (' + when + ')')
            }
          } else {
            appendPre('No upcoming events found.');
          }
        });
      }

    </script>

    <script async defer src="https://apis.google.com/js/api.js"
      onload="this.onload=function(){};handleClientLoad()"
      onreadystatechange="if (this.readyState === 'complete') this.onload()">
    </script>

    <script>
    	$(document).ready(function() {

		    // page is now ready, initialize the calendar...

		    $('#calendar').fullCalendar({
		        // put your options and callbacks here
		        googleCalendarApiKey : 'AIzaSyDbbQ-FxT378bBPFyee3fQwsyhNV04KLKM',
		        events: {
		        	googleCalendarId: 'carlos.cazares.89@gmail.com',
		        	color : '#800000',
              className: 'gcalevent'
		        },
		      	views: {
		      		month: {
		      			displayEventTime: 'true',
		        		

		      		},
		      		agendaWeek: {
		      			displayEventTime: 'true',
		        		displayEventEnd: 'true'
		      		},
		      		agendaDay: {
		      			displayEventTime: 'true',
		        		displayEventEnd: 'true'
		      		},
		      		listWeek: {
		      			displayEventTime: 'true',
		        		displayEventEnd: 'true'
		      		}
		      	},
		        header: {
			        left: 'prev,next today',
			        center: 'title',
			        right: 'month,agendaWeek,agendaDay,listWeek'
			    },
			    navLinks: false, // can click day/week names to navigate views<
			    eventLimitText: true,
			    /*eventMouseover: function(calEvent, jsEvent, view) {

			        alert('Evento: ' + calEvent.title + '\n' + 'Inicio: ' + calEvent.start + '\n' + 'Fin: ' + calEvent.End) ;

			        // change the border color just for fun
			        $(this).css('border-color', 'red');

			    }*/
			     eventClick: function(event) {
        // opens events in a popup window
            window.open(event.url, 'gcalevent', 'width=800,height=600');
            return false;
          },
          loading: function(bool) {
            $('#loading').toggle(bool);
          }
		  });

		});

    </script>

    
    
    <div id='loading'>loading...</div>
    <div id = "calendar"></div>	

   <iframe src="https://calendar.google.com/calendar/embed?showPrint=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23cc0000&amp;src=carlos.cazares.89%40gmail.com&amp;color=%23711616&amp;ctz=America%2FMexico_City" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
  </body>
</html>
